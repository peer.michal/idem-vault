import pytest
from pytest_idem.runner import run_yaml_block

# Parametrization options for running each test with --test first and then without --test
PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])

SECRET_PATH = "secret/idem_test"

PRESENT_STATE = f"""
test_resource:
  vault.secrets.kv_v1.secret.present:
    - {{resource_path}}: {SECRET_PATH}
    - data:
        mapping:
          key: value
""".rstrip()

ABSENT_STATE = f"""
test_resource:
  vault.secrets.kv_v1.secret.absent:
    - {{resource_path}}: {SECRET_PATH}
""".rstrip()

SEARCH_STATE = f"""
{PRESENT_STATE}

verify:
  exec.run:
    - path: vault.secrets.kv_v2.secret.get
    - kwargs:
      path: {SECRET_PATH}
"""


@pytest.fixture(scope="module", autouse=True)
async def skip_kv_v1(version):
    if version != "v1":
        raise pytest.skip("Only runs on kv_v1")


@pytest.mark.parametrize(**PARAMETRIZE)
def test_starting_absent(esm_cache, acct_data, __test, version, resource_path):
    """
    Verify that the absent state is successful for a resource that never existed
    """
    STATE = ABSENT_STATE.format(resource_path=resource_path)
    # Run the state defined in the yaml block
    ret = run_yaml_block(
        STATE, managed_state=esm_cache, acct_data=acct_data, test=__test
    )
    resource_ret = next(iter(ret.values()))
    assert resource_ret["result"], "\n".join(resource_ret["comment"])

    assert resource_ret["new_state"] is None


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
def test_present(hub, ctx, esm_cache, acct_data, __test, version, resource_path):
    """
    Create a brand new secret based on an image that exists locally
    """
    STATE = PRESENT_STATE.format(resource_path=resource_path)
    # Run the state defined in the yaml block
    ret = run_yaml_block(
        STATE, managed_state=esm_cache, acct_data=acct_data, test=__test
    )
    resource_ret = next(iter(ret.values()))
    assert resource_ret["result"], "\n".join(resource_ret["comment"])
    assert resource_ret["changes"]
    assert resource_ret["changes"]["new"]
    new_changes = resource_ret["changes"]["new"]
    for change_key in new_changes:
        assert "*" == new_changes[change_key]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present_no_update", depends=["present"])
def test_present_no_update(
    hub, ctx, esm_cache, acct_data, __test, version, resource_path
):
    """
    Create a brand new secret based on an image that exists locally
    """
    # Run the state defined in the yaml block
    STATE = PRESENT_STATE.format(resource_path=resource_path)
    ret = run_yaml_block(
        STATE, managed_state=esm_cache, acct_data=acct_data, test=__test
    )
    resource_ret = next(iter(ret.values()))
    assert resource_ret["result"], "\n".join(resource_ret["comment"])
    assert resource_ret["old_state"] and resource_ret["new_state"]
    assert resource_ret["old_state"] == resource_ret["new_state"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(depends=["present_no_update"])
def test_already_present(esm_cache, acct_data, __test, version, resource_path):
    """
    The secret should exist in the cache, verify that the resource id gets populated and no changes are made
    """
    # Run the state defined in the yaml block
    STATE = PRESENT_STATE.format(resource_path=resource_path)
    ret = run_yaml_block(
        STATE, managed_state=esm_cache, acct_data=acct_data, test=__test
    )
    resource_ret = next(iter(ret.values()))
    assert resource_ret["result"], "\n".join(resource_ret["comment"])

    if not __test:
        assert not resource_ret["changes"], resource_ret["changes"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(depends=["present_no_update"])
def test_absent(esm_cache, acct_data, __test, version, resource_path):
    """
    Terminate the secret
    """
    if version == "v2":
        return
    # Run the state defined in the yaml block

    STATE = ABSENT_STATE.format(resource_path=resource_path)
    ret = run_yaml_block(
        STATE, managed_state=esm_cache, acct_data=acct_data, test=__test
    )
    resource_ret = next(iter(ret.values()))
    assert resource_ret["result"], "\n".join(resource_ret["comment"])
    assert resource_ret["new_state"] is None


@pytest.mark.parametrize(**PARAMETRIZE)
def test_already_absent(esm_cache, acct_data, __test, version, resource_path):
    """
    Verify that termination is idempotent
    """
    if version == "v2":
        return
    # Run the state defined in the yaml block
    STATE = ABSENT_STATE.format(resource_path=resource_path)
    ret = run_yaml_block(
        STATE, managed_state=esm_cache, acct_data=acct_data, test=__test
    )
    resource_ret = next(iter(ret.values()))
    assert resource_ret["result"], resource_ret["comment"]
    assert resource_ret["new_state"] is None
